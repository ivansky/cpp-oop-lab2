if [ ! -f "./CMakeCache.txt" ]
then
    cmake ./
    echo "CMake cache has configured"
fi

cmake --build ./ --target all

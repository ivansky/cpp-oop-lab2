#ifndef OOPLAB2_TRIANGLE_H
#define OOPLAB2_TRIANGLE_H

#include "Figure.hpp"

class Triangle : public Figure {
    float calculateArea() override;
    const char* getName() override;
};

#endif //OOPLAB2_TRIANGLE_H

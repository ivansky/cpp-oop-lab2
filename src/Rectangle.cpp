#include "Rectangle.h"

float Rectangle::calculateArea() {
    return getWidth() * getHeight();
};

const char* Rectangle::getName() {
    return "Rectangle";
};

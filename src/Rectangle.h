#ifndef OOPLAB2_RECTANGLE_H
#define OOPLAB2_RECTANGLE_H

#include "Figure.hpp"

class Rectangle : public Figure {
    float calculateArea() override;
    const char* getName() override;
};


#endif //OOPLAB2_RECTANGLE_H

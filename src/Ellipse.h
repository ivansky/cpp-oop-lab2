#ifndef OOPLAB2_ELLIPSE_H
#define OOPLAB2_ELLIPSE_H


#include "Figure.hpp"

class Ellipse : public Figure {
    float calculateArea() override;
    const char* getName() override;
};


#endif //OOPLAB2_ELLIPSE_H

#include "Triangle.h"

float Triangle::calculateArea() {
    return getWidth() * getHeight() / 2;
};

const char* Triangle::getName() {
    return "Triangle";
};

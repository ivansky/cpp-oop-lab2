#ifndef OOPLAB2_FIGURE_H
#define OOPLAB2_FIGURE_H

enum Figures {
    Rectangle,
    Triangle,
    Ellipse
};

class Figure {
private:
    float width = 0.0f;
    float height = 0.0f;
public:
    void setWidth(float);
    void setHeight(float);
    float getWidth();
    float getHeight();
    static Figure* createRandomFigure();
    virtual const char* getName() = 0;
    virtual float calculateArea() = 0;
};


#endif //OOPLAB2_FIGURE_H

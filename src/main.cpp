#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Figure.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    srand(time(0)); //initialize the random seed

    auto a = Figure::createRandomFigure();
    a->setWidth(rand() % 10 + 1);
    a->setHeight(rand() % 10 + 1);

    auto b = Figure::createRandomFigure();
    b->setWidth(rand() % 10 + 1);
    b->setHeight(rand() % 10 + 1);

    cout << "A. " << a->getName() << ", Width: " << a->getWidth() << ", Height: " << a->getHeight() << endl;
    cout << "B. " << b->getName() << ", Width: " << b->getWidth() << ", Height: " << b->getHeight() << endl;

    cout << "Summary: ";

    if (a->calculateArea() == b->calculateArea()) {
        cout << "A figure area equal B figure area" << endl;
    } else if (a->calculateArea() > b->calculateArea()) {
        cout << "A figure area > B figure area" << endl;
    } else {
        cout << "A figure area < B figure area" << endl;
    }

    return 0;
}
const float PI_F = 3.14159265358979f;

#include "Ellipse.h"

float Ellipse::calculateArea() {
    return getWidth() * getHeight() * PI_F;
};

const char* Ellipse::getName() {
    return "Ellipse";
};

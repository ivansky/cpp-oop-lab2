#include <cstdlib>
#include "Figure.hpp"
#include "Rectangle.h"
#include "Ellipse.h"
#include "Triangle.h"

void Figure::setWidth(float _width) {
    width = _width;
}

void Figure::setHeight(float _height) {
    height = _height;
}

float Figure::getWidth() {
    return width;
}

float Figure::getHeight() {
    return height;
}

Figure* Figure::createRandomFigure() {
    const int figures[3] = {
            Figures::Rectangle,
            Figures::Ellipse,
            Figures::Triangle
    };

    const int randomIndex = rand() % 3;

    switch(figures[randomIndex]) {
        case Figures::Rectangle: return static_cast<Figure*>(new class Rectangle());
        case Figures::Ellipse: return static_cast<Figure*>(new class Ellipse());
        case Figures::Triangle: return static_cast<Figure*>(new class Triangle());
        default: throw "Did not recognize a figure";
    }
}

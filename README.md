# Laboratory work №2

## Necessary tools
- [**CMake**](https://cmake.org/download/)
- Compiler *(one of them)*
    * [**GCC 6 +**](https://gcc.gnu.org/install/binaries.html)
    * [**CLang 3.5 +**](http://releases.llvm.org/download.html)
    * [**MSVC 19 +**](http://landinghub.visualstudio.com/visual-cpp-build-tools)

## Build
Windows Cmd

`$ ./build.bat`

Unix / MacOS terminal

`$ chmod +x ./build.sh && ./build.sh`


## Run
Windows Cmd

`$ ./main.exe`

Unix / MacOS terminal

`$ ./main`

## Entities
 * Figure abstract class
 * Ellipse class
 * Rectangle class
 * Triangle class 

## Task

Using OOP principles we should construct an application.

Application should create random figure: Triangle, Rectangle or Ellipse.

Then it should calculate area and compare.

After all comparing information should be printed on the display/console.

## Author

© Ivan Martianov
